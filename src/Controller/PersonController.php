<?php
/**
 * PersonController.php
 *
 * API Controller
 *
 * @category   Controller
 * @package    Test
 * @author     Manuel Telleria
 */

namespace App\Controller;

use App\Entity\Person;
use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class PersonController
 *
 * @Route("/api")
 */
class PersonController extends FOSRestController
{
    // Person URI's

    /**
     * @Rest\Get("/v1/persons.{_format}", name="persons", defaults={"_format":"json"})
     *
     * Description: Gets all persons.
     * Params: none
     *
     * Output: Success: json { 'code': 200
     *                         'error': false,
     *  		       'data': [ list ]
     *                       }
     *
     *         Failed: json { 'code': 500,
     *                        'error': true,
     *                        'message' ''
     *                      }
     *
     *
     */
    public function getAllPersonAction(Request $request) {
        $serializer = $this->get('jms_serializer');
        $em = $this->getDoctrine()->getManager();
        $persons = [];
        $message = "";

        try {
            $code = 200;
            $error = false;

            $persons = $em->getRepository("App:Person")->findAll();

            if (is_null($persons)) {
                $persons = [];
            }

        } catch (Exception $ex) {
            $code = 500;
            $error = true;
            $message = "An error has occurred trying to get all Persons - Error: {$ex->getMessage()}";
        }

        $response = [
            'code' => $code,
            'error' => $error,
            'data' => $code == 200 ? $persons : $message,
        ];

        return new Response($serializer->serialize($response, "json"));
    }

    /**
     * @Rest\Post("/v1/person.{_format}", name="person_add", defaults={"_format":"json"})
     *
     * Description: EndPoint that is used to register a person in the system 
     * Params:
     *           id_document: Number of document Id
     *           name: Name of the person to be registered
     *           address: address of the person to be registered
     *           phone: phone of the person to be registered
     *
     * Output: Success: json { 'code': 200
     *                         'error': false,
     *  		       'data':  { id_document: '',
     *  		                  name: '',
     *  		                  address: '',
     *  		                  phone: ''
     *                                  }
     *                       }
     *
     *         Failed: json { 'code': 500,
     *                        'error': true,
     *                        'message' ''
     *                      }
     *
     */
    public function addPersonAction(Request $request) {
        $serializer = $this->get('jms_serializer');
        $em = $this->getDoctrine()->getManager();
        $person = [];
        $message = "";

        try {
           $code = 201;
           $error = false;
           $idDocument = $request->request->get("id_document", null);
           $name = $request->request->get("name", null);
           $address = $request->request->get("address", null);
	   $phone = $request->request->get("phone", null);

           if (!is_null($idDocument) && !is_null($name)) {
               $person = new Person();
               $person->setId_document($idDocument);
               $person->setName($name);
               $person->setAddress($address);
               $person->setPhone($phone);

               $em->persist($person);
               $em->flush();

           } else {
               $code = 500;
               $error = true;
               $message = "An error has occurred trying to add new board - Error: You must to provide a cleint name and Id document";
           }

        } catch (Exception $ex) {
            $code = 500;
            $error = true;
            $message = "An error has occurred trying to add new person - Error: {$ex->getMessage()}";
        }

        $response = [
            'code' => $code,
            'error' => $error,
            'data' => $code == 201 ? $person : $message,
        ];

        return new Response($serializer->serialize($response, "json"));
    }
}
