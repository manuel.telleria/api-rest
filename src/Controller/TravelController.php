<?php
/**
 * TravelController.php
 *
 * API Controller
 *
 * @category   Controller
 * @package    MyKanban
 * @author     Francisco Ugalde
 * @copyright  2018 www.franciscougalde.com
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */

namespace App\Controller;

use App\Entity\Travel;
use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class TravelController
 *
 * @Route("/api")
 */
class TravelController extends FOSRestController
{
    // Client URI's

    /**
     * @Rest\Get("/v1/travels.{_format}", name="travels", defaults={"_format":"json"})
     *
     * Description: Gets all clients.
     *
     * Output: Success: json { 'code': 200
     *                         'error': false,
     *  		       'data': [ list ]
     *                       }
     *
     *         Failed: json { 'code': 500,
     *                        'error': true,
     *                        'message' ''
     *                      }
     *
     *
     */
    public function getAllTravelAction(Request $request) {
        $serializer = $this->get('jms_serializer');
        $em = $this->getDoctrine()->getManager();
        $travels = [];
        $message = "";

        try {
            $code = 200;
            $error = false;

            $travels = $em->getRepository("App:Travel")->findAll();

            if (is_null($travels)) {
                $travels = [];
            }

        } catch (Exception $ex) {
            $code = 500;
            $error = true;
            $message = "An error has occurred trying to get all Travels - Error: {$ex->getMessage()}";
        }

        $response = [
            'code' => $code,
            'error' => $error,
            'data' => $code == 200 ? $travels : $message,
        ];

        return new Response($serializer->serialize($response, "json"));
    }

    /**
     * @Rest\Post("/v1/travel.{_format}", name="travel_add", defaults={"_format":"json"})
     *
     * Description: EndPoint that is used to register a travel in the system 
     *
     * Output: Success: json { 'code': 200
     *                         'error': false,
     *  		       'data':  { code: '',
     *  		                  capacity: '',
     *  		                  destination: '',
     *  		                  origin: '',
     *  		                  price: ''
     *                                  }
     *                       }
     *
     *         Failed: json { 'code': 500,
     *                        'error': true,
     *                        'message' ''
     *                      }
     *
     */
    public function addTravelAction(Request $request) {
        $serializer = $this->get('jms_serializer');
        $em = $this->getDoctrine()->getManager();
        $travel = [];
        $message = "";

        try {
           $code = 201;
           $error = false;
           $code = $request->request->get("code", null);
           $capacity = $request->request->get("capacity", null);
           $destination = $request->request->get("destination", null);
	   $origin = $request->request->get("origin", null);
	   $price = $request->request->get("price", null);

           if (!is_null($code)) {
               $travel = new Travel();
               $travel->setCode($code);
               $travel->setCapacity($capacity);
               $travel->setDestination($destination);
               $travel->setOrigin($origin);
               $travel->setPrice($price);

               $em->persist($travel);
               $em->flush();

           } else {
               $code = 500;
               $error = true;
               $message = "An error has occurred trying to add new board - Error: You must to provide a cleint name and Id document";
           }

        } catch (Exception $ex) {
            $code = 500;
            $error = true;
            $message = "An error has occurred trying to add new client - Error: {$ex->getMessage()}";
        }

        $response = [
            'code' => $code,
            'error' => $error,
            'data' => $code == 201 ? $travel : $message,
        ];

        return new Response($serializer->serialize($response, "json"));
    }
}
